/**
 * Loads of code taken from Zerthox at https://github.com/Zerthox/BetterDiscord-Plugins/blob/84e1d6af58dced5aa5396ea9d27aa3a3c972f94b/dist/bd/BetterFolders.plugin.js
 * 
 * @name FolderBackgroundColor
 * @author GuillaumeVDN
 * @description Changes the background color of open folders to the same color as their icon.
 * @version 0.0.1
 */

// ----- find/patch utilities

const find = (filter, { resolve = true, entries = false } = {}) => BdApi.Webpack.getModule(filter, {
  defaultExport: resolve,
  searchExports: entries
});
const byName = (name, options) => find(byName$1(name), options);
const byName$1 = (name) => {
    return (target) => (target?.displayName ?? target?.constructor?.displayName) === name;
};
const byProps = (props, options) => find(byProps$1(...props), options);
const byProps$1 = (...props) => {
    return (target) => target instanceof Object && props.every((prop) => prop in target);
};

const patch = (type, object, method, callback, options) => {
  const original = object?.[method];
  if (!(original instanceof Function)) {
      throw TypeError(`patch target ${original} is not a function`);
  }
  const cancel = BdApi.Patcher[type]("FolderBackgroundColor", object, method, options.once ? (...args) => {
      const result = callback(cancel, original, ...args);
      cancel();
      return result;
  } : (...args) => callback(cancel, original, ...args));
  if (!options.silent) {
      console.log(`Patched ${options.name ?? String(method)}`);
  }
  return cancel;
};
const after = (object, method, callback, options = {}) => patch("after", object, method, (cancel, original, context, args, result) => callback({ cancel, original, context, args, result }), options);

// ----- js utilities

function intToRGBA(num) {  // https://stackoverflow.com/a/11866980
  num >>>= 0;
  var b = num & 0xFF,
      g = (num & 0xFF00) >>> 8,
      r = (num & 0xFF0000) >>> 16,
      a = ( (num & 0xFF000000) >>> 24 ) / 255;
  return "rgba(" + [r, g, b, .5].join(",") + ")";
}

// ----- folder utilities

const ToggleGuildFolderExpand = byProps(["toggleGuildFolderExpand"]);
const ExpandedGuildFolderStore = byName("ExpandedGuildFolderStore");
const SortedGuildUtils = byProps(["getSortedGuilds", "getFlattenedGuildIds"]);

function getFolderById(id) {
  return SortedGuildUtils.guildFolders.filter(n => n.folderId).find(n => n.folderId == id);
}

function isFolderExpanded(id) {
  return ExpandedGuildFolderStore.getExpandedFolders().has(id);
}

function setFolderStyle(folderId) {
  let color = getFolderById(folderId)?.folderColor ?? "0";
  let expanded = isFolderExpanded(folderId);

  let folderItems = document.getElementById("folder-items-" + folderId);
  let backgroundSpan = folderItems.parentElement.getElementsByTagName("span")[0];  // conveniently, there's currently only one span in that div
  backgroundSpan.style = `
    background-color: ${intToRGBA(color)};
    margin-left: ${expanded ? '-5px' : '0px'};
    width: ${expanded ? '58px' : '48px'};
  `;
}

// ----- plugin

module.exports = class FolderBackgroundColorPlugin {
    start() {
      // set style of expanded folders
      for (let folderId of ExpandedGuildFolderStore.getExpandedFolders()) {
        setFolderStyle(folderId);
      }

      // set style on folder expand
      after(ToggleGuildFolderExpand, "toggleGuildFolderExpand", ({ original, args: [folderId] }) => {
        setFolderStyle(folderId);
      });
    }

    stop() {}
}
